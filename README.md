hexibits is a simple web page allowing to display an animated carousel of different medias sources(images, videos, web pages).

# Usage

* Provide a list of the items you want to display in items.js 
* Add and organize the resources you need in the assets folder
* Open index.html in a web browser
* Enjoy! :)