////////////
// Data
////////////
/*
Format:

{
    "title": "",
    "duration": 10,
    "type" : "image | video",
    "source" : "assets/item.xyz"
}
    where 
        - [optional] title should be maximum 75 chars long
        - [optional] duration is in seconds, default is 10 seconds. 
          For video, the duration is computed from the length of the video.
        - source is the path to the resource(videos must be in .mp4)

*/

const items = [
    {
        "type": "image",
        "duration": 5,
        "source": "assets/demo-image.png"
    },
    {
        "title": "You can add a title",
        "type": "video",
        "source": "assets/demo-video.mp4"
    },
    {
        "title": "Load a remote web page...",
        "duration": 5,
        "type": "web",
        "source": "http://fast.com/"
    },
    {
        "title": "or a local HTML document!",
        "duration": 5,
        "type": "web",
        "source": "assets/demo-html.html"
    }
]